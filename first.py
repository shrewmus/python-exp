import pygame
import pygame.camera
from screens.manager import Manager
from screens.art_board import ArtBoard
from screens.art_timer import ArtTimer


class Webcam:
    is_started = False

    def __init__(self):
        pygame.camera.init()
        camlist = pygame.camera.list_cameras()
        # self.webcam = pygame.camera.Camera(camlist[0], (480, 640))
        self.webcam = pygame.camera.Camera(camlist[0], (1440, 1080), "RGB")

    def start(self):
        self.is_started = True
        self.webcam.start()

    def stop(self):
        self.is_started = False
        self.webcam.stop()

    def set_draw_surface(self, draw_surface):
        self.draw_surface = draw_surface

    def update(self):
        if self.draw_surface:
            if self.is_started:
                img = self.webcam.get_image()
                img = pygame.transform.scale(img, (1920, 1080))
                img2 = img.subsurface(pygame.Rect(660, 0, 600, 1024))
                img = pygame.transform.rotate(img2, 90)
                pygame.image.save(img, 'test.jpg')
                size = img.get_size()
                surf = self.draw_surface.get_size()
                self.draw_surface.blit(img, (surf[0] / 2 - size[0] / 2, surf[1] / 2 - size[1] / 2))


class Main:
    art_board = None
    art_timmer = None

    def start(self):
        self.manager = Manager()
        self.manager.toggle_rotated()
        self.camera = Webcam()
        self.art_board = ArtBoard(manager=self.manager)
        self.art_board.set_button_callback(self.change_to_timer)
        self.manager.change_screen(new_screen=self.art_board)
        self.art_board.set_camera(webcam=self.camera)
        self.camera.start()
        self.manager.run()

    def change_to_timer(self):
        self.art_board.set_camera(None)
        if not self.art_timmer:
            self.art_timmer = ArtTimer(self.manager)

        self.art_timmer.set_camera(webcam=self.camera)
        self.manager.change_screen(self.art_timmer)
        self.art_timmer.start_timer()


if __name__ == '__main__':
    pygame.init()
    pygame.font.init()
    pygame.camera.init()
    main = Main()
    main.start()
