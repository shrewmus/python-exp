import pygame


class ClickableRectangle:
    callback = None
    is_mouse_down = False
    size = None

    def __init__(self, pos, size):
        self.size = size
        self.rect = pygame.Rect((0, 0), size)
        self.rect.center = pos
        self.has_clicked = False

    def set_click_callback(self, callback):
        self.callback = callback

    def is_mouse_over(self):
        curpos = pygame.mouse.get_pos()
        is_over = self.rect.left < curpos[0] < self.rect.right and self.rect.top < curpos[1] < self.rect.bottom
        return is_over

    def handle_click(self):
        if self.callback:
            if self.is_mouse_over():
                for event in pygame.event.get():
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        self.is_mouse_down = True
                    if event.type == pygame.MOUSEBUTTONUP and self.is_mouse_down:
                        self.is_mouse_down = False
                        return self.callback()

    def update(self):
        self.handle_click()


class Button(ClickableRectangle):

    def __init__(self, pos, size, color, screen):
        ClickableRectangle.__init__(self, pos, size)
        self.screen = screen
        self.image = pygame.Surface(size, pygame.SRCALPHA, 16)
        self.image.fill(color)

    def draw(self):
        self.screen.blit(self.image, self.rect)

    def update(self):
        self.draw()
        ClickableRectangle.update(self)


class TextButton(Button):
    def __init__(self, pos, size, color, text, screen):
        self.text = pygame.font.Font(None, 30)
        self.text = self.text.render(text, True, pygame.Color(0, 0, 0))
        self.textRect = self.text.get_rect()
        # Button.__init__(self, (textRect.width, textRect.height + 10), (textRect.width + 20, textRect.height + 20),
        #                 color, screen)
        Button.__init__(self, pos, (self.textRect.width + 20, self.textRect.height + 20),
                        color, screen)
        self.imsize = self.image.get_rect()

    def draw(self):
        # print((self.size[0] / 2 - self.textRect.width / 2, self.size[1] + self.textRect.height / 2))
        print(self.imsize.width / 2 - self.textRect.width / 2, self.imsize.height + self.textRect.height / 2)
        # print(self.imsize)
        self.image.blit(self.text, (self.imsize.width / 2 - self.textRect.width / 2, self.imsize.height + self.textRect.height / 2))
        Button.draw(self)
