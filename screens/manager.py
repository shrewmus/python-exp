import pygame


class Manager:
    app_time = None
    font = None
    rotated = False

    def __init__(self):
        self.screen = pygame.display.set_mode((1024, 600))
        self.app_screen = None
        self.is_quit = False
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 30)

    def toggle_rotated(self):
        self.rotated = not self.rotated

    def run(self):
        """ Main loop """

        while not self.is_quit:
            self.app_time = self.clock.tick(30)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.quit()

            self.app_screen.on_event()

            self.app_screen.on_update()

            self.app_screen.on_draw(self.screen)

            pygame.display.flip()

    def change_screen(self, new_screen):
        self.app_screen = new_screen

    def quit(self):
        self.is_quit = True
