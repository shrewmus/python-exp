import pygame
from screens.screen import Screen
from screens.button import TextButton


class ArtBoard(Screen):
    callback = None

    def __init__(self, manager):
        Screen.__init__(self, manager)
        # manager.screen.set_alpha(128)
        self.button = TextButton((100, 100), (100, 100), pygame.Color(0, 0, 0, 50), 'Hello', manager.screen)
        self.button.set_click_callback(lambda: manager.change_screen())

        self.text1 = pygame.font.SysFont(name="Courier New", size=30) \
            .render('Big label text', True, pygame.Color(0, 0, 0))
        if manager.rotated:
            self.text1 = pygame.transform.rotate(self.text1, 90)
            # self.button.image = pygame.transform.rotate(self.button.image, 90)
            # self.button.text = pygame.transform.rotate(self.button.text, 90)

    def set_button_callback(self, callback):
        # self.callback = callback
        self.button.set_click_callback(callback)

    def set_camera(self, webcam):
        self.webcam = webcam
        if webcam:
            webcam.set_draw_surface(self.manager.screen)

    def on_update(self):
        if self.webcam:
            self.webcam.update()
        self.button.update()
        self.manager.screen.blit(self.text1, (150, 150))

    def on_event(self):
        pass

    def on_draw(self, screen):
        pass
