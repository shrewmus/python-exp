import pygame

from screens.screen import Screen


class ArtTimer(Screen):
    max = 6000
    start = 0
    current = 0
    is_running = False

    def __init__(self, manager):
        Screen.__init__(self, manager)
        self.text1 = pygame.font.SysFont(name="Courier New", size=30) \
            .render('Big label for TIMER screen', True, pygame.Color(0, 0, 0))
        if manager.rotated:
            self.text1 = pygame.transform.rotate(self.text1, 90)

    def start_timer(self):
        if self.current == 0:
            self.is_running = True
            self.start = pygame.time.get_ticks() // 1000

    def set_camera(self, webcam):
        self.webcam = webcam
        if webcam:
            webcam.set_draw_surface(self.manager.screen)

    def on_update(self):

        if self.current > self.max:
            self.current = 0
            self.is_running = False
        else:
            if self.is_running:
                cur_sec = pygame.time.get_ticks() // 1000
                self.current = cur_sec - self.start

        if self.webcam:
            self.webcam.update()

        self.text2 = pygame.font.SysFont(name="Courier New", size=40) \
            .render(str(self.current), True, pygame.Color(0, 0, 0))

        self.manager.screen.blit(self.text1, (150, 150))
        self.manager.screen.blit(self.text2, (150, 50))

    def on_event(self):
        pass

    def on_draw(self, screen):
        pass
