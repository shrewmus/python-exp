class Screen:
    def __init__(self, manager):
        self.manager = manager

    def on_update(self):
        raise NotImplementedError('call abstract method')

    def on_event(self):
        raise NotImplementedError('call abstract method')

    def on_draw(self, screen):
        raise NotImplementedError('call abstract method')
